import styled from "styled-components/macro";

import bgImg from "../../img/bg.jpg";

export const StyledTetrisWrapper = styled.div`
  width: 100vw;
  height: 100vh;
  background: url(${bgImg}) #000;
  background-size: cover;
  overflow: hidden;
`;
//45: 24
export const StyledTetris = styled.div`
  display: flex;
  align-items: flex-start;
  padding: 40px;
  margin: 0 auto;
  max-width: 900px;

  aside {
    width: 100%;
    max-width: 200px;
    display: blox;
    padding: 0 20px;
  }
`;
