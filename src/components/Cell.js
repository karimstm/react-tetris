import React from "react";
import { StyledCell } from "./styles/StyledCell";
import { TETROMINOS } from "../tetrominos";

function Cell({ type }) {
  return <StyledCell color={TETROMINOS[type].color} type={type} />;
}

export default Cell;
